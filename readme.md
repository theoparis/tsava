# Typed Ava Assertions

[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)
[![Linting By ESLint](https://raw.githubusercontent.com/aleen42/badges/master/src/eslint.svg)](https://eslint.org)
[![Typescript](https://raw.githubusercontent.com/aleen42/badges/master/src/typescript.svg)](https://typescriptlang.org)

## Introduction

I have created this because I could not find a single test runner that supports typed assertion functions with Typescript. This lead to some nasty workarounds with duplicate code. This is inspired by [ava's closed issue on this topic.](https://github.com/avajs/ava/issues/2449).

## Example

```typescript
import test from 'ava'
import { assertIs } from '@theoparis/tsava'

test('Example test', async (t) => {
  const parsingResult = {
    // ...
  }

  assertIs(parsingResult.isError, false)
  t.deepEqual(parsingResult.result, {
    // ...
  })
})
```
